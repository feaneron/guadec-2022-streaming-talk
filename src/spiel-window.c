/* spiel-window.c
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "spiel-window.h"

#include "spiel-buffer.h"
#include "spiel-media-gallery-view.h"
#include "spiel-preferences-window.h"
#include "spiel-presentation-controller.h"
#include "spiel-presentation-window.h"
#include "spiel-presenter-notes-view.h"
#include "spiel-project-private.h"
#include "spiel-slide-rendition.h"
#include "spiel-source-view.h"
#include "spiel-theme-selector.h"

#include <glib/gi18n.h>

struct _SpielWindow
{
  AdwApplicationWindow  parent_instance;

  AdwBin *current_view;
  AdwOverlaySplitView *overlay_split_view;
  AdwSplitButton *present_split_button;
  GtkEditable *project_title_editable_label;
  GtkScrolledWindow *scrolled_window;
  GtkSingleSelection *slides_selection_model;
  GtkStack *stack;

  SpielProject *project;
  SpielSourceView *source_view;
  GCancellable *cancellable;
  GMenu *monitors_menu;

  SpielPresentationWindow *presentation;
};

G_DEFINE_FINAL_TYPE (SpielWindow, spiel_window, ADW_TYPE_APPLICATION_WINDOW)

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * Auxiliary methods
 */

static void
rebuild_monitors_menu (SpielWindow *self)
{
  GdkDisplay *display = gdk_display_get_default ();
  GListModel *monitors = gdk_display_get_monitors (display);

  g_menu_remove_all (self->monitors_menu);

  for (size_t i = 0; i < g_list_model_get_n_items (monitors); i++)
    {
      g_autoptr (GdkMonitor) monitor = g_list_model_get_item (monitors, i);
      g_autofree char *action = g_strdup_printf ("win.present-at(%lu)", i);


      g_message ("Action: %s", action);

      {
        g_autoptr (GVariant) target_value = NULL;
        g_autofree char *action_name = NULL;

        g_action_parse_detailed_name (action, &action_name, &target_value, NULL);

        g_message ("Action name: %s, value: %s", action_name, g_variant_print (target_value, TRUE));
      }

      g_menu_append (self->monitors_menu, gdk_monitor_get_description (monitor), action);
    }
}


/*
 * Callbacks
 */

static void
on_monitors_items_changed_cb (GListModel   *model,
                              unsigned int  position,
                              unsigned int  removed,
                              unsigned int  added,
                              SpielWindow  *self)
{
  g_assert (SPIEL_IS_WINDOW (self));

  rebuild_monitors_menu (self);
}

static void
on_buffer_saved_cb (GObject      *source,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_autoptr (GError) error = NULL;
  SpielWindow *self;

  self = user_data;
  g_assert (SPIEL_IS_WINDOW (self));

  if (!spiel_buffer_save_finish (SPIEL_BUFFER (source), result, &error))
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        g_warning ("Error saving buffer: %s", error->message);
      g_clear_object (&self->cancellable);
      return;
    }

  g_clear_object (&self->cancellable);
}

static void
on_about_action_activated_cb (GSimpleAction *action,
                              GVariant      *parameter,
                              gpointer       user_data)
{
  SpielWindow *self;

  self = user_data;
  g_assert (SPIEL_IS_WINDOW (self));

  adw_show_about_dialog (GTK_WIDGET (self),
                         "application-name", "Spiel",
                         "application-icon", "com.feaneron.Spiel",
                         "developer-name", "Georges Basile Stavracas Neto",
                         "version", "0.1.0",
                         "developers", (const char *[]) {
                           "Georges Basile Stavracas Neto <georges.stavracas@gmail.com>",
                           NULL,
                         },
                         "copyright", "© 2024 Georges Stavracas",
                         NULL);
}

static void
presentation_destroyed_cb (gpointer  data,
                           GObject  *where_the_object_was)
{
  SpielWindow *self = (SpielWindow *) data;

  g_assert (SPIEL_IS_WINDOW (self));

  self->presentation = NULL;

  adw_bin_set_child (self->current_view, GTK_WIDGET (self->overlay_split_view));

  /* Release the ref acquired by present_at_monitor() */
  g_object_unref (self->overlay_split_view);
}

static void
present_at_monitor (SpielWindow *self,
                    int          monitor_index)
{
  gboolean created = FALSE;

  g_assert (SPIEL_IS_WINDOW (self));
  g_assert (monitor_index >= -1);

  if (!self->presentation)
    {
      g_autoptr (SpielPresentationController) controller = NULL;
      g_autoptr (GtkWidget) presenter_notes_view = NULL;
      uint32_t selected;

      selected = gtk_single_selection_get_selected (self->slides_selection_model);
      controller = spiel_presentation_controller_new (self->project, selected);

      self->presentation = spiel_presentation_window_new (controller);
      g_object_weak_ref (G_OBJECT (self->presentation),
                         presentation_destroyed_cb,
                         self);

      presenter_notes_view = spiel_presenter_notes_view_new (controller);
      g_object_ref_sink (presenter_notes_view);

      /* Acquire the ref to prevent self->overlay_split_view from being
       * destroyed. It'll be released at presentation_destroyed_cb().
       */
      g_object_ref (self->overlay_split_view);

      adw_bin_set_child (self->current_view, presenter_notes_view);

      g_object_bind_property (self->slides_selection_model, "selected",
                              controller, "current-slide",
                              G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);

      created = TRUE;
    }

  gtk_window_present (GTK_WINDOW (self->presentation));

  if (created)
    {
      if (monitor_index >= 0)
        {
          g_autoptr (GdkMonitor) monitor = NULL;
          GListModel *monitors;
          GdkDisplay *display;

          display = gdk_display_get_default ();
          monitors = gdk_display_get_monitors (display);
          monitor = g_list_model_get_item (monitors, monitor_index);

          gtk_window_fullscreen_on_monitor (GTK_WINDOW (self->presentation), monitor);
        }
      else
        {
          gtk_window_fullscreen (GTK_WINDOW (self->presentation));
        }
    }
}

static void
on_present_at_action_activated_cb (GSimpleAction *action,
                                   GVariant      *parameter,
                                   gpointer       user_data)
{
  SpielWindow *self = (SpielWindow *) user_data;

  g_assert (SPIEL_IS_WINDOW (self));

  present_at_monitor (self, g_variant_get_int32 (parameter));
}

static void
on_present_action_activated_cb (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
  SpielWindow *self = (SpielWindow *) user_data;

  g_assert (SPIEL_IS_WINDOW (self));

  present_at_monitor (self, -1);
}

static void
on_save_action_activated_cb (GSimpleAction *action,
                             GVariant      *parameter,
                             gpointer       user_data)
{
  GtkTextBuffer *buffer;
  SpielWindow *self;

  self = user_data;

  g_assert (self->source_view != NULL);

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (self->source_view));
  g_assert (SPIEL_IS_BUFFER (buffer));

  if (!gtk_text_buffer_get_modified (buffer))
    {
      g_debug ("File not modified, skipping save");
      return;
    }

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_debug ("Saving project %s", spiel_project_get_id (self->project));

  self->cancellable = g_cancellable_new ();
  spiel_buffer_save (SPIEL_BUFFER (buffer),
                     self->cancellable,
                     on_buffer_saved_cb,
                     self);
}

static void
on_stop_presenting_action_activated_cb (GSimpleAction *action,
                                        GVariant      *parameter,
                                        gpointer       user_data)
{
  SpielWindow *self = user_data;

  if (self->presentation)
    {
      g_debug ("Stopping presentation");
      gtk_window_destroy (GTK_WINDOW (self->presentation));
    }
}

static void
on_show_preferences_action_activated_cb (GSimpleAction *action,
                                         GVariant      *parameter,
                                         gpointer       user_data)
{
  SpielWindow *self;
  AdwDialog *dialog;

  self = user_data;
  g_assert (SPIEL_IS_WINDOW (self));

  dialog = spiel_preferences_window_new ();
  adw_dialog_present (dialog, GTK_WIDGET (self));
}

static char *
position_to_string (gpointer     object,
                    unsigned int position)
{
  return g_strdup_printf ("%u", position + 1);
}


/*
 * GObject overrides
 */

static void
spiel_window_dispose (GObject *object)
{
  SpielWindow *self = (SpielWindow *)object;

  if (self->project)
    spiel_project_unload_markdown (self->project);

  g_clear_object (&self->project);

  /* FIXME: this is ugly */
  while (self->cancellable != NULL)
    g_main_context_iteration (NULL, TRUE);

  G_OBJECT_CLASS (spiel_window_parent_class)->dispose (object);
}

static void
spiel_window_constructed (GObject *object)
{
  SpielMediaGallery *media_gallery;
  SpielWindow *self;
  GListModel *slides;
  GtkWidget *media_gallery_view;

  self = (SpielWindow *)object;

  G_OBJECT_CLASS (spiel_window_parent_class)->constructed (object);

  g_assert (self->project != NULL);

  self->source_view = SPIEL_SOURCE_VIEW (spiel_source_view_new (self->project));
  gtk_scrolled_window_set_child (self->scrolled_window, GTK_WIDGET (self->source_view));

  slides = spiel_project_get_slides (self->project);
  g_assert (slides != NULL);
  gtk_single_selection_set_model (self->slides_selection_model, slides);

  g_object_bind_property (self->project, "name",
                          self->project_title_editable_label, "text",
                          G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);

  /* Media gallery view */
  media_gallery = spiel_project_get_media_gallery (self->project);
  media_gallery_view = spiel_media_gallery_view_new (media_gallery);
  gtk_stack_add_titled (self->stack , media_gallery_view, "gallery", _("Gallery"));
  gtk_stack_page_set_icon_name (gtk_stack_get_page (self->stack, media_gallery_view), "image-x-generic-symbolic");
}

static void
spiel_window_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  SpielWindow *self = SPIEL_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, self->project);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_window_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  SpielWindow *self = SPIEL_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_assert (self->project == NULL);
      self->project = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_window_class_init (SpielWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (SPIEL_TYPE_THEME_SELECTOR);
  g_type_ensure (SPIEL_TYPE_SLIDE_RENDITION);
  g_type_ensure (SPIEL_TYPE_SOURCE_VIEW);

  object_class->dispose = spiel_window_dispose;
  object_class->constructed = spiel_window_constructed;
  object_class->get_property = spiel_window_get_property;
  object_class->set_property = spiel_window_set_property;

  properties[PROP_PROJECT] =
    g_param_spec_object ("project", "", "",
                         SPIEL_TYPE_PROJECT,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-window.ui");

  gtk_widget_class_bind_template_child (widget_class, SpielWindow, current_view);
  gtk_widget_class_bind_template_child (widget_class, SpielWindow, overlay_split_view);
  gtk_widget_class_bind_template_child (widget_class, SpielWindow, present_split_button);
  gtk_widget_class_bind_template_child (widget_class, SpielWindow, project_title_editable_label);
  gtk_widget_class_bind_template_child (widget_class, SpielWindow, scrolled_window);
  gtk_widget_class_bind_template_child (widget_class, SpielWindow, slides_selection_model);
  gtk_widget_class_bind_template_child (widget_class, SpielWindow, stack);

  gtk_widget_class_bind_template_callback (widget_class, position_to_string);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK, "win.save", NULL);
}

static void
spiel_window_init (SpielWindow *self)
{
  const GActionEntry actions[] = {
    { "about", on_about_action_activated_cb },
    { "present", on_present_action_activated_cb },
    { "present-at", on_present_at_action_activated_cb, "i" },
    { "save", on_save_action_activated_cb },
    { "stop-presenting", on_stop_presenting_action_activated_cb },
    { "show-preferences", on_show_preferences_action_activated_cb },
  };
  GListModel *monitors;
  GdkDisplay *display;

  gtk_widget_init_template (GTK_WIDGET (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   actions,
                                   G_N_ELEMENTS (actions),
                                   self);

  self->monitors_menu = g_menu_new ();
  adw_split_button_set_menu_model (self->present_split_button, G_MENU_MODEL (self->monitors_menu));

  display = gdk_display_get_default ();
  monitors = gdk_display_get_monitors (display);
  g_signal_connect_object (monitors, "items-changed", G_CALLBACK (on_monitors_items_changed_cb), self, 0);

  rebuild_monitors_menu (self);
}

GtkWindow *
spiel_window_new (SpielApplication *application,
                  SpielProject     *project)
{
  return g_object_new (SPIEL_TYPE_WINDOW,
                       "application", application,
                       "project", project,
                       NULL);
}

SpielProject *
spiel_window_get_project (SpielWindow *self)
{
  g_return_val_if_fail (SPIEL_IS_WINDOW (self), NULL);

  return self->project;
}
