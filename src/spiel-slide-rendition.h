/*
 * spiel-slide-rendition.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "spiel-types.h"

G_BEGIN_DECLS

typedef enum
{
  SPIEL_SLIDE_RENDITION_THUMBNAIL,
  SPIEL_SLIDE_RENDITION_PRESENTATION,
} SpielSlideRenditionType;

#define SPIEL_TYPE_SLIDE_RENDITION (spiel_slide_rendition_get_type())
G_DECLARE_FINAL_TYPE (SpielSlideRendition, spiel_slide_rendition, SPIEL, SLIDE_RENDITION, GtkWidget)

SpielSlideRenditionType
spiel_slide_rendition_get_rendition_type (SpielSlideRendition     *self);

void
spiel_slide_rendition_set_rendition_type (SpielSlideRendition     *self,
                                          SpielSlideRenditionType  rendition_type);

SpielSlide * spiel_slide_rendition_get_slide (SpielSlideRendition *self);
void         spiel_slide_rendition_set_slide (SpielSlideRendition *self,
                                              SpielSlide          *slide);

G_END_DECLS
