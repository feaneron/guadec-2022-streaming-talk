/*
 * spiel-project.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtksourceview/gtksource.h>

#include "spiel-types.h"

G_BEGIN_DECLS

typedef enum {
  SPIEL_PROJECT_STATE_UNLOADED,
  SPIEL_PROJECT_STATE_LOADING,
  SPIEL_PROJECT_STATE_LOADED,
} SpielProjectState;

#define SPIEL_TYPE_PROJECT (spiel_project_get_type())
G_DECLARE_FINAL_TYPE (SpielProject, spiel_project, SPIEL, PROJECT, GObject)

const char * spiel_project_get_id (SpielProject *self);

SpielProjectState spiel_project_get_state (SpielProject *self);

const char * spiel_project_get_name (SpielProject *self);
void         spiel_project_set_name (SpielProject *self,
                                     const char   *name);

SpielBuffer * spiel_project_get_buffer (SpielProject *self);

void          spiel_project_unload_markdown (SpielProject *self);

void     spiel_project_load        (SpielProject         *self,
                                    GCancellable         *cancellable,
                                    GAsyncReadyCallback   callback,
                                    gpointer              user_data);

gboolean spiel_project_load_finish (SpielProject         *self,
                                    GAsyncResult         *result,
                                    GError              **error);

GListModel * spiel_project_get_slides (SpielProject *self);

G_END_DECLS
