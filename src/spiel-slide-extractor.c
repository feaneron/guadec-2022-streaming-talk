/*
 * spiel-slide-extractor.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-extractor.h"

#include "spiel-buffer.h"
#include "spiel-slide.h"

struct _SpielSlideExtractor
{
  GObject parent_instance;

  GtkTextBuffer *buffer;

  GListStore *slides_store;
};

G_DEFINE_FINAL_TYPE (SpielSlideExtractor, spiel_slide_extractor, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_BUFFER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

typedef struct
{
  char *text;
  GtkTextIter start;
  GtkTextIter end;
} SlideCandidate;

static void
slide_candidade_free (gpointer data)
{
  SlideCandidate *candidate = data;
  g_clear_pointer (&candidate->text, g_free);
  g_free (candidate);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SlideCandidate, slide_candidade_free)

typedef enum
{
  INSERT,
  DELETE,
  SUBSTITUTE,
} EditOperation;

typedef struct
{
  EditOperation op;
  uint32_t position;
  uint32_t other_position;
} Edit;

static GArray *
calculate_editing_plan_for_slides (SpielSlideExtractor *self,
                                   GPtrArray           *candidates)
{
  g_autoptr (GArray) edits = NULL;
  int64_t current_len;
  int64_t goal_len;

  current_len = g_list_model_get_n_items (G_LIST_MODEL (self->slides_store));
  goal_len = candidates->len;

  edits = g_array_sized_new (FALSE, FALSE,
                             sizeof (Edit),
                             MAX (current_len, goal_len));

  if (current_len != 0 && goal_len != 0)
    {
      g_autofree uint32_t *distances = NULL;

#define IDX(row, col) ((candidates->len + 1) * (row) + (col))

      /* First, tabulate all distances in a matrix */
      g_debug ("Allocating array with size %ld for distances", (current_len + 1) * (goal_len + 1));
      distances = g_malloc0 (sizeof (uint32_t) * (current_len + 1) * (goal_len + 1) * 2);

      for (uint32_t i = 0; i < current_len + 1; i++)
        distances[IDX (i, 0)] = i;

      for (uint32_t j = 0; j < goal_len + 1; j++)
        distances[IDX (0, j)] = j;

      g_debug ("Calculating distances");
      for (uint32_t i = 1; i < current_len + 1; i++)
        {
          for (uint32_t j = 1; j < goal_len + 1; j++)
            {
              g_autoptr (SpielSlide) slide = NULL;
              SlideCandidate *candidate = NULL;

              slide = g_list_model_get_item (G_LIST_MODEL (self->slides_store), i - 1);
              candidate = g_ptr_array_index (candidates, j - 1);

              g_assert (slide != NULL);
              g_assert (candidate != NULL && candidate->text != NULL);

              if (g_strcmp0 (spiel_slide_get_text (slide), candidate->text) == 0)
                {
                  distances[IDX (i, j)] = distances[IDX (i - 1, j - 1)];
                }
              else
                {
                  distances[IDX (i, j)] = MIN (distances[IDX (i - 1, j)] + 1,
                                               MIN (distances[IDX (i, j - 1)] + 1,
                                                    distances[IDX (i - 1, j - 1)] + 1));
                }
            }
        }

#if 0
      g_message ("Edit distances:");
      for (guint i = 0; i < current_len + 1; i++)
        {
          for (guint j = 0; j < goal_len + 1; j++)
              g_print ("%u  ", distances[IDX (i, j)]);
          g_print ("\n");
        }
#endif

      /* Then walk backwards on the matrix to figure out what were
       * the editing operations, by comparing the editing distances.
       */
      while (MAX (current_len, goal_len) > 0)
        {
          uint32_t current_distance;
          uint32_t replace_distance;
          uint32_t delete_distance;
          uint32_t insert_distance;
          uint32_t minimum;

          current_distance = distances[IDX (current_len, goal_len)];

          replace_distance = current_len > 0 && goal_len > 0
            ? distances[IDX (current_len - 1, goal_len - 1)]
            : G_MAXUINT32;

          delete_distance = current_len > 0
            ? distances[IDX (current_len - 1, goal_len)]
            : G_MAXUINT32;

          insert_distance = goal_len > 0
            ? distances[IDX (current_len, goal_len - 1)]
            : G_MAXUINT32;

          minimum = MIN (replace_distance, MIN (delete_distance, insert_distance));

          if (minimum == current_distance)
            {
              current_len--;
              goal_len--;
            }
          else
            {
              Edit edit;

              g_assert (minimum == current_distance - 1);

              if (minimum == insert_distance)
                {
                  edit.op = INSERT;
                  edit.position = current_len;
                  edit.other_position = --goal_len;
                }
              else if (minimum == delete_distance)
                {
                  edit.op = DELETE;
                  edit.position = --current_len;
                  edit.other_position = G_MAXUINT32;
                }
              else if (minimum == replace_distance)
                {
                  edit.op = SUBSTITUTE;
                  edit.position = --current_len;
                  edit.other_position = --goal_len;
                }
              else
                {
                  g_assert_not_reached ();
                }

              g_array_append_val (edits, edit);
            }
        }

#undef IDX
    }
  else if (current_len == 0)
    {
      for (int64_t i = 0; i < goal_len; i++)
        {
          g_array_append_vals (edits,
                               &(Edit) {
                                 .op = INSERT,
                                 .position = i,
                                 .other_position = i,
                               }, 1);
        }
    }
  else
    {
      for (size_t i = 0; i < current_len; i++)
        {
          g_array_append_vals (edits,
                               &(Edit) {
                                 .op = DELETE,
                                 .position = i,
                                 .other_position = 0,
                               }, 1);
        }
    }

  return g_steal_pointer (&edits);
}


/*
 * Buffer segmenter
 */

typedef enum
{
  SLIDE,
  CODE,
} SegmenterSection;

typedef struct
{
  SegmenterSection section;
  GtkTextIter start;
  GtkTextIter end;
} SegmenterState;

static gboolean
line_changes_section (const GtkTextIter *line_start,
                      SegmenterSection  *out_section)
{
  SegmenterSection candidate = -1;
  GtkTextIter aux;
  uint32_t count = 0;
  gboolean was_whitespace = FALSE;

  g_assert (gtk_text_iter_starts_line (line_start));

  aux = *line_start;

  /* Advance to the first non-whitespace character */
  while (!gtk_text_iter_ends_line (&aux) &&
         gtk_text_iter_get_char (&aux) == ' ' &&
         gtk_text_iter_forward_char (&aux))
    ;

  while (!gtk_text_iter_ends_line (&aux))
    {
      SegmenterSection char_candidate = -1;
      gunichar iter_char;

      iter_char = gtk_text_iter_get_char (&aux);

      /* Unknown char */
      if (iter_char == 0xFFC)
        return FALSE;

      switch (iter_char)
        {
        case '-':
          g_debug ("  Candidate: SLIDE");
          char_candidate = SLIDE;
          break;

        case '`':
          g_debug ("  Candidate: CODE");
          char_candidate = CODE;
          break;

        case ' ':
          was_whitespace = TRUE;
          goto forward_char;

        default:
          if (candidate == CODE)
            goto forward_char;
          g_debug ("Return default case");
          return FALSE;
        }

      if (was_whitespace)
        {
          g_debug ("  Quitting due to whitespace");
          return FALSE;
        }

      if (candidate == -1)
        candidate = char_candidate;

      if (candidate != char_candidate)
        {
          g_debug ("  Quitting due to candidate mismatch");
          return FALSE;
        }

      count++;

forward_char:
      if (!gtk_text_iter_forward_char (&aux))
        break;
    }

  if (count < 3)
    return FALSE;

  *out_section = candidate;
  return TRUE;
}

static inline void
push_segmenter_state (GArray            *state_stack,
                      SegmenterSection   section,
                      const GtkTextIter *start)
{
  SegmenterState state = {
    .section = section,
    .start = *start,
    .end = *start,
  };

  g_array_append_val (state_stack, state);
}

static inline SegmenterState
pop_segmenter_state (GArray *state_stack)
{
  SegmenterState current_state;

  /* Don't use pointers here, since we want a copy of this state */
  current_state = g_array_index (state_stack, SegmenterState, state_stack->len - 1);

  g_array_remove_index (state_stack, state_stack->len - 1);

  if (state_stack->len > 0)
   {
     SegmenterState *next_state;

     next_state = &g_array_index (state_stack, SegmenterState, state_stack->len - 1);
     next_state->end = current_state.end;
   }

  return current_state;
}

static GPtrArray *
segment_buffer (SpielSlideExtractor *self)
{
  g_autoptr (GPtrArray) candidates = NULL;
  g_autoptr (GArray) state_stack = NULL;
  GtkTextIter line_start;
  uint32_t line = 0;

  state_stack = g_array_new (FALSE, TRUE, sizeof (SegmenterState));
  candidates = g_ptr_array_new_full (100, slide_candidade_free);

  gtk_text_buffer_get_start_iter (self->buffer, &line_start);

  /* Initial state is SLIDE */
  push_segmenter_state (state_stack, SLIDE, &line_start);

  do
    {
      SegmenterSection new_section = -1;
      SegmenterState *current_state;

      g_assert (state_stack->len > 0);

      g_debug ("Checking line %u", line++);

      current_state = &g_array_index (state_stack, SegmenterState, state_stack->len - 1);

      if (!line_changes_section (&line_start, &new_section))
        {
          g_debug ("  Line does not change segment");
          current_state->end = line_start;
          if (!gtk_text_iter_ends_line (&current_state->end))
            gtk_text_iter_forward_to_line_end (&current_state->end);
          continue;
        }

      switch (new_section)
        {
        case SLIDE:
          {
            SegmenterState popped_state;

            g_debug ("  Current state: %d", current_state->section);

            if (current_state->section == CODE)
              continue;

            popped_state = pop_segmenter_state (state_stack);
            g_debug ("  Popped state: %d", popped_state.section);
            if (popped_state.section == SLIDE)
              {
                g_autoptr (SlideCandidate) candidate = NULL;
                g_autofree char *text = NULL;

                text = gtk_text_buffer_get_text (self->buffer,
                                                 &popped_state.start,
                                                 &popped_state.end,
                                                 FALSE);

                g_debug ("    Appending slide");

                candidate = g_new0 (SlideCandidate, 1);
                candidate->text = g_steal_pointer (&text);
                candidate->start = popped_state.start;
                candidate->end = popped_state.end;

                g_ptr_array_add (candidates, g_steal_pointer (&candidate));
              }
            push_segmenter_state (state_stack, SLIDE, &line_start);
          }
          break;

        case CODE:
          {
            if (current_state->section != CODE)
              push_segmenter_state (state_stack, CODE, &line_start);
            else
              pop_segmenter_state (state_stack);
          }
          break;

        default:
          g_assert_not_reached ();
        }
    }
  while (gtk_text_iter_forward_line (&line_start));

  g_assert (state_stack->len > 0);

  /* Pop remaining state */
  while (state_stack->len > 0)
    {
      SegmenterState popped_state = pop_segmenter_state (state_stack);

      switch (popped_state.section)
        {
        case SLIDE:
          {
            g_autoptr (SlideCandidate) candidate = NULL;
            g_autofree char *text = NULL;

            /* There must never be a slide inside a slide */
            g_assert (state_stack->len == 0);

            text = gtk_text_buffer_get_text (self->buffer,
                                             &popped_state.start,
                                             &popped_state.end,
                                             FALSE);

            g_debug ("    Appending slide");

            candidate = g_new0 (SlideCandidate, 1);
            candidate->text = g_steal_pointer (&text);
            candidate->start = popped_state.start;
            candidate->end = popped_state.end;

            g_ptr_array_add (candidates, g_steal_pointer (&candidate));
          }
          break;

        case CODE:
        default:
          break;
        }
    }

  return g_steal_pointer (&candidates);
}

static void
apply_edits (SpielSlideExtractor *self,
             GArray              *edits,
             GPtrArray           *candidates)
{

  for (guint i = 0; i < edits->len; i++)
    {
      const Edit *edit = &g_array_index (edits, Edit, i);

      g_debug ("Edit %d: %s  %u  %u", i,
               edit->op == INSERT ? "insert" : edit->op == DELETE ? "delete" : "substitute",
               edit->position,
               edit->other_position);

      switch (edit->op)
        {
        case INSERT:
          {
            g_autoptr (GtkTextMark) start_mark = NULL;
            g_autoptr (GtkTextMark) end_mark = NULL;
            g_autoptr (SpielSlide) slide = NULL;
            SlideCandidate *candidate;

            candidate = g_ptr_array_index (candidates, edit->other_position);

            slide = spiel_slide_new (candidate->text);
            start_mark = gtk_text_buffer_create_mark (self->buffer, NULL, &candidate->start, TRUE);
            end_mark = gtk_text_buffer_create_mark (self->buffer, NULL, &candidate->end, FALSE);
            spiel_slide_set_marks (slide, g_steal_pointer (&start_mark), g_steal_pointer (&end_mark));

            g_list_store_insert (self->slides_store, edit->position, slide);
          }
          break;

        case DELETE:
          {
            g_autoptr (SpielSlide) slide = NULL;
            GtkTextMark *start_mark;
            GtkTextMark *end_mark;

            slide = g_list_model_get_item (G_LIST_MODEL (self->slides_store), edit->position);
            spiel_slide_get_marks (slide, &start_mark, &end_mark);
            gtk_text_buffer_delete_mark (self->buffer, start_mark);
            gtk_text_buffer_delete_mark (self->buffer, end_mark);

            g_list_store_remove (self->slides_store, edit->position);
          }
          break;

        case SUBSTITUTE:
          {
            g_autoptr (GtkTextMark) start_mark = NULL;
            g_autoptr (GtkTextMark) end_mark = NULL;
            g_autoptr (SpielSlide) slide = NULL;
            SlideCandidate *candidate;
            GtkTextMark *old_start_mark;
            GtkTextMark *old_end_mark;

            candidate = g_ptr_array_index (candidates, edit->other_position);

            slide = g_list_model_get_item (G_LIST_MODEL (self->slides_store), edit->position);

            spiel_slide_get_marks (slide, &old_start_mark, &old_end_mark);
            gtk_text_buffer_delete_mark (self->buffer, old_start_mark);
            gtk_text_buffer_delete_mark (self->buffer, old_end_mark);

            spiel_slide_set_text (slide, candidate->text);
            start_mark = gtk_text_buffer_create_mark (self->buffer, NULL, &candidate->start, TRUE);
            end_mark = gtk_text_buffer_create_mark (self->buffer, NULL, &candidate->end, FALSE);
            spiel_slide_set_marks (slide, g_steal_pointer (&start_mark), g_steal_pointer (&end_mark));
          }
          break;

        default:
          g_assert_not_reached ();
        }
    }
}

static void
extract_slides (SpielSlideExtractor *self)
{
  g_autoptr (GPtrArray) new_slides = NULL;
  g_autoptr (GArray) edits = NULL;

  g_assert (self->buffer != NULL);
  g_assert (!gtk_source_buffer_get_loading (GTK_SOURCE_BUFFER (self->buffer)));

  new_slides = segment_buffer (self);
  edits = calculate_editing_plan_for_slides (self, new_slides);

  apply_edits (self, edits, new_slides);
}

/*
 * Callbacks
 */

static void
on_buffer_changed_cb (SpielBuffer         *buffer,
                      SpielSlideExtractor *self)
{
  if (!gtk_source_buffer_get_loading (GTK_SOURCE_BUFFER (buffer)))
    extract_slides (self);
}

static void
on_buffer_loading_changed_cb (SpielBuffer         *buffer,
                              GParamSpec          *pspec,
                              SpielSlideExtractor *self)
{
  if (!gtk_source_buffer_get_loading (GTK_SOURCE_BUFFER (self->buffer)))
    extract_slides (self);
}

/*
 * GObject overrides
 */

static void
spiel_slide_extractor_dispose (GObject *object)
{
  SpielSlideExtractor *self = (SpielSlideExtractor *)object;

  g_clear_object (&self->slides_store);
  g_clear_object (&self->buffer);

  G_OBJECT_CLASS (spiel_slide_extractor_parent_class)->dispose (object);
}

static void
spiel_slide_extractor_constructed (GObject *object)
{
  SpielSlideExtractor *self = (SpielSlideExtractor *)object;

  G_OBJECT_CLASS (spiel_slide_extractor_parent_class)->constructed (object);

  if (!gtk_source_buffer_get_loading (GTK_SOURCE_BUFFER (self->buffer)))
    extract_slides (self);
}

static void
spiel_slide_extractor_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  SpielSlideExtractor *self = SPIEL_SLIDE_EXTRACTOR (object);

  switch (prop_id)
    {
    case PROP_BUFFER:
      g_value_set_object (value, self->buffer);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_extractor_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  SpielSlideExtractor *self = SPIEL_SLIDE_EXTRACTOR (object);

  switch (prop_id)
    {
    case PROP_BUFFER:
      g_assert (self->buffer == NULL);
      self->buffer = g_value_dup_object (value);
      g_signal_connect (self->buffer,
                        "notify::loading",
                        G_CALLBACK (on_buffer_loading_changed_cb),
                        self);
      g_signal_connect (self->buffer,
                        "changed",
                        G_CALLBACK (on_buffer_changed_cb),
                        self);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_extractor_class_init (SpielSlideExtractorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = spiel_slide_extractor_dispose;
  object_class->constructed = spiel_slide_extractor_constructed;
  object_class->get_property = spiel_slide_extractor_get_property;
  object_class->set_property = spiel_slide_extractor_set_property;

  properties[PROP_BUFFER] =
    g_param_spec_object ("buffer", "", "",
                         SPIEL_TYPE_BUFFER,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_slide_extractor_init (SpielSlideExtractor *self)
{
  self->slides_store = g_list_store_new (SPIEL_TYPE_SLIDE);
}

SpielSlideExtractor *
spiel_slide_extractor_new (SpielBuffer *buffer)
{
  return g_object_new (SPIEL_TYPE_SLIDE_EXTRACTOR,
                       "buffer", buffer,
                       NULL);
}

GListModel *
spiel_slide_extractor_get_slides (SpielSlideExtractor *self)
{
  g_return_val_if_fail (SPIEL_IS_SLIDE_EXTRACTOR (self), NULL);

  return G_LIST_MODEL (self->slides_store);
}
