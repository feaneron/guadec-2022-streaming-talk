/*
 * spiel-greeter-row.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-greeter-row.h"

#include "spiel-project.h"

struct _SpielGreeterRow
{
  AdwActionRow parent_instance;

  SpielProject *project;
  gboolean selected;
};

G_DEFINE_FINAL_TYPE (SpielGreeterRow, spiel_greeter_row, ADW_TYPE_ACTION_ROW)

enum {
  PROP_0,
  PROP_SELECTED,
  PROP_PROJECT,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * Callbacks
 */

static void
on_gesture_click_pressed_cb (GtkGestureClick *click_gesture,
                             int              n_presses,
                             double           x,
                             double           y,
                             SpielGreeterRow *self)
{
  gtk_list_box_row_set_selectable (GTK_LIST_BOX_ROW (self), TRUE);
  g_signal_emit_by_name (self, "activate");
}


/*
 * GObject overrides
 */

static void
spiel_greeter_row_dispose (GObject *object)
{
  SpielGreeterRow *self = (SpielGreeterRow *)object;

  g_clear_object (&self->project);

  G_OBJECT_CLASS (spiel_greeter_row_parent_class)->dispose (object);
}

static void
spiel_greeter_row_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  SpielGreeterRow *self = SPIEL_GREETER_ROW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, self->project);
      break;

    case PROP_SELECTED:
      g_value_set_boolean (value, self->selected);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_greeter_row_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  SpielGreeterRow *self = SPIEL_GREETER_ROW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_assert (self->project == NULL);
      self->project = g_value_dup_object (value);
      g_object_bind_property (self->project, "name",
                              self, "title",
                              G_BINDING_SYNC_CREATE);
      g_object_bind_property (self->project, "id",
                              self, "subtitle",
                              G_BINDING_SYNC_CREATE);
      break;

    case PROP_SELECTED:
      spiel_greeter_row_set_selected (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_greeter_row_class_init (SpielGreeterRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = spiel_greeter_row_dispose;
  object_class->get_property = spiel_greeter_row_get_property;
  object_class->set_property = spiel_greeter_row_set_property;

  properties[PROP_PROJECT] = g_param_spec_object ("project", "", "",
                                                  SPIEL_TYPE_PROJECT,
                                                  G_PARAM_READWRITE |
                                                  G_PARAM_CONSTRUCT_ONLY |
                                                  G_PARAM_STATIC_STRINGS);

  properties[PROP_SELECTED] =
    g_param_spec_boolean ("selected", "", "",
                          FALSE,
                          G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-greeter-row.ui");

  gtk_widget_class_bind_template_callback (widget_class, on_gesture_click_pressed_cb);
}

static void
spiel_greeter_row_init (SpielGreeterRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
spiel_greeter_row_new (SpielProject *project)
{
  return g_object_new (SPIEL_TYPE_GREETER_ROW,
                       "project", project,
                       NULL);
}

SpielProject *
spiel_greeter_row_get_project (SpielGreeterRow *self)
{
  g_return_val_if_fail (SPIEL_IS_GREETER_ROW (self), NULL);

  return self->project;
}

gboolean
spiel_greeter_row_get_selected (SpielGreeterRow *self)
{
  g_return_val_if_fail (SPIEL_IS_GREETER_ROW (self), FALSE);

  return self->selected;
}

void
spiel_greeter_row_set_selected (SpielGreeterRow *self,
                                gboolean         selected)
{
  g_return_if_fail (SPIEL_IS_GREETER_ROW (self));

  if (self->selected == selected)
    return;

  self->selected = selected;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SELECTED]);
}
