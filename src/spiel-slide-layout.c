/*
 * spiel-slide-layout.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-layout.h"

G_DEFINE_INTERFACE (SpielSlideLayout, spiel_slide_layout, GTK_TYPE_WIDGET)

static gboolean
spiel_slide_layout_default_apply (SpielSlideLayout        *self,
                                  const SpielSlideFeature *features[])
{
  return TRUE;
}

static void
spiel_slide_layout_default_init (SpielSlideLayoutInterface *iface)
{
  iface->apply = spiel_slide_layout_default_apply;
}

/**
 * spiel_slide_layout_apply:
 * @self: a #SpielSlideLayout
 * @features: (array zero-terminated=1): array of #SpielSlideFeature objects
 *
 * Returns: whether @self was able to apply @features
 */
gboolean
spiel_slide_layout_apply (SpielSlideLayout        *self,
                          const SpielSlideFeature *features[])
{
  g_return_val_if_fail (SPIEL_IS_SLIDE_LAYOUT (self), FALSE);
  g_return_val_if_fail (features != NULL, FALSE);
  g_return_val_if_fail (SPIEL_SLIDE_LAYOUT_GET_IFACE (self)->apply, FALSE);

  return SPIEL_SLIDE_LAYOUT_GET_IFACE (self)->apply (self, features);
}
