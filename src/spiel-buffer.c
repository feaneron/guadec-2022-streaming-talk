/*
 * spiel-buffer.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-buffer.h"

#include "spiel-application.h"
#include "spiel-project-private.h"

#include <adwaita.h>

#define DEFAULT_TEXT \
"---\n"              \
"\n"                 \
"# Title 1\n"        \
"## Subtitle 1\n"    \
"\n"                 \
"Speaker notes\n"    \
"\n"                 \
"---\n"              \
"\n"                 \
"# Title 2\n"        \
"## Subtitle 2\n"    \
"\n"                 \
"Speaker notes\n"    \
"\n"                 \
"---\n"              \
"\n"                 \
"# Title 3\n"        \
"## Subtitle 3\n"    \
"\n"                 \
"Speaker notes\n"    \
"\n"                 \
"---\n"              \
"\n"                 \
"# Title 4\n"        \
"## Subtitle 4\n"    \
"\n"                 \
"Speaker notes"

struct _SpielBuffer
{
  GtkSourceBuffer parent_instance;

  GSettings *settings;
  SpielProject *project;
};

G_DEFINE_FINAL_TYPE (SpielBuffer, spiel_buffer, GTK_SOURCE_TYPE_BUFFER)

enum {
  PROP_0,
  PROP_MODIFIED,
  PROP_PROJECT,
  N_PROPS,
};

static GParamSpec *properties[N_PROPS] = { NULL, };


/*
 * Auxiliary functions
 */

static void
apply_style_scheme (SpielBuffer *self)
{
  GtkSourceStyleSchemeManager *scheme_manager;
  GtkSourceStyleScheme *scheme;
  AdwStyleManager *style_manager;
  const char *current_scheme;

  current_scheme = g_settings_get_string (self->settings, "style-scheme");
  style_manager = adw_style_manager_get_default ();
  scheme_manager = gtk_source_style_scheme_manager_get_default ();

  if (adw_style_manager_get_dark (style_manager))
    {
      GtkSourceStyleScheme *light_scheme;
      const char *dark_variant;

      light_scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, current_scheme);
      dark_variant = gtk_source_style_scheme_get_metadata (light_scheme, "dark-variant");
      scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, dark_variant);
    }
  else
    {
      scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, current_scheme);
    }

  gtk_source_buffer_set_style_scheme (GTK_SOURCE_BUFFER (self), scheme);
}


/*
 * Callbacks
 */

static void
markdown_file_load_progress_cb (goffset  current_num_bytes,
                                goffset  total_num_bytes,
                                gpointer user_data)
{
  /* TODO: report progress */
}

static void
markdown_file_save_progress_cb (goffset  current_num_bytes,
                                goffset  total_num_bytes,
                                gpointer user_data)
{
  /* TODO: report progress */
}

static void
markdown_file_loaded_cb (GObject      *source,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;

  if (!gtk_source_file_loader_load_finish (GTK_SOURCE_FILE_LOADER (source),
                                           result,
                                           &error))
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        g_warning ("Error loading Markdown file: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (task, TRUE);
}

static void
markdown_file_saved_cb (GObject      *source,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = NULL;

  task = user_data; /* invalid if cancelled */

  if (!gtk_source_file_saver_save_finish (GTK_SOURCE_FILE_SAVER (source),
                                          result,
                                          &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (task, TRUE);
}


static void
on_settings_style_scheme_changed_cb (GSettings   *settings,
                                     const char  *key,
                                     SpielBuffer *self)
{
  apply_style_scheme (self);
}

static void
on_style_manager_dark_changed_cb (GObject     *object,
                                  GParamSpec  *pspec,
                                  SpielBuffer *self)
{
  apply_style_scheme (self);
}



/*
 * GtkTextBuffer overrides
 */

static void
spiel_buffer_modified_changed (GtkTextBuffer *buffer)
{
  g_object_notify_by_pspec (G_OBJECT (buffer), properties[PROP_MODIFIED]);
}


/*
 * GObject overrides
 */

static void
spiel_buffer_dispose (GObject *object)
{
  SpielBuffer *self = (SpielBuffer *)object;

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (spiel_buffer_parent_class)->dispose (object);
}

static void
spiel_buffer_constructed (GObject *object)
{
  SpielBuffer *self = (SpielBuffer *)object;

  G_OBJECT_CLASS (spiel_buffer_parent_class)->constructed (object);

  apply_style_scheme (self);
}

static void
spiel_buffer_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  SpielBuffer *self = SPIEL_BUFFER (object);

  switch (prop_id)
    {
    case PROP_MODIFIED:
      g_value_set_boolean (value, gtk_text_buffer_get_modified (GTK_TEXT_BUFFER (self)));
      break;

    case PROP_PROJECT:
      g_value_set_object (value, self->project);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_buffer_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  SpielBuffer *self = SPIEL_BUFFER (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_assert (self->project == NULL);
      self->project = g_value_get_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_buffer_class_init (SpielBufferClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkTextBufferClass *text_buffer_class = GTK_TEXT_BUFFER_CLASS (klass);

  object_class->dispose = spiel_buffer_dispose;
  object_class->constructed = spiel_buffer_constructed;
  object_class->get_property = spiel_buffer_get_property;
  object_class->set_property = spiel_buffer_set_property;

  text_buffer_class->modified_changed = spiel_buffer_modified_changed;

  properties[PROP_MODIFIED] = g_param_spec_boolean ("modified", "", "",
                                                    FALSE,
                                                    G_PARAM_READABLE |
                                                    G_PARAM_STATIC_STRINGS);

  properties[PROP_PROJECT] = g_param_spec_object ("project", "", "",
                                                  SPIEL_TYPE_PROJECT,
                                                  G_PARAM_READWRITE |
                                                  G_PARAM_CONSTRUCT_ONLY |
                                                  G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_buffer_init (SpielBuffer *self)
{
  GtkSourceLanguageManager *language_manager;
  GtkSourceLanguage *markdown;
  SpielApplication *application;

  language_manager = gtk_source_language_manager_get_default ();
  markdown = gtk_source_language_manager_get_language (language_manager, "markdown");
  gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (self), markdown);

  application = SPIEL_APPLICATION (g_application_get_default ());
  self->settings = spiel_application_get_settings (application);
  g_signal_connect_object (self->settings,
                           "changed::style-scheme",
                           G_CALLBACK (on_settings_style_scheme_changed_cb),
                           self,
                           0);

  g_signal_connect_object (adw_style_manager_get_default (),
                           "notify::dark",
                           G_CALLBACK (on_style_manager_dark_changed_cb),
                           self,
                           0);
}

GtkTextBuffer *
spiel_buffer_new (SpielProject *project)
{
  return g_object_new (SPIEL_TYPE_BUFFER,
                       "project", project,
                       NULL);
}

GtkTextBuffer *
spiel_buffer_new_empty (void)
{
  g_autoptr (GtkTextBuffer) buffer = NULL;

  buffer = g_object_new (SPIEL_TYPE_BUFFER, NULL);
  gtk_text_buffer_set_text (buffer, DEFAULT_TEXT, -1);

  return g_steal_pointer (&buffer);
}

void
spiel_buffer_load (SpielBuffer         *self,
                   GCancellable        *cancellable,
                   GAsyncReadyCallback  callback,
                   gpointer             user_data)
{
  g_autoptr (GtkSourceFileLoader) loader = NULL;
  g_autoptr (GTask) task = NULL;
  GtkSourceFile *source_file;

  g_return_if_fail (SPIEL_IS_BUFFER (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_buffer_load);

  if (!self->project)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  source_file = spiel_project_get_markdown_file (self->project);
  loader = gtk_source_file_loader_new (GTK_SOURCE_BUFFER (self), source_file);

  gtk_source_file_loader_load_async (loader,
                                     G_PRIORITY_HIGH_IDLE,
                                     cancellable,
                                     markdown_file_load_progress_cb,
                                     self, NULL,
                                     markdown_file_loaded_cb,
                                     g_steal_pointer (&task));
}

gboolean
spiel_buffer_load_finish (SpielBuffer   *self,
                          GAsyncResult  *result,
                          GError       **error)
{
  g_return_val_if_fail (!error || !*error, FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_buffer_load, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
spiel_buffer_save (SpielBuffer         *self,
                   GCancellable        *cancellable,
                   GAsyncReadyCallback  callback,
                   gpointer             user_data)
{
  g_autoptr (GtkSourceFileSaver) saver = NULL;
  g_autoptr (GTask) task = NULL;
  GtkSourceFile *source_file;

  g_return_if_fail (SPIEL_IS_BUFFER (self));
  g_return_if_fail (self->project != NULL);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_buffer_save);

  source_file = spiel_project_get_markdown_file (self->project);
  saver = gtk_source_file_saver_new (GTK_SOURCE_BUFFER (self), source_file);

  gtk_source_file_saver_save_async (saver,
                                    G_PRIORITY_DEFAULT,
                                    cancellable,
                                    markdown_file_save_progress_cb,
                                    self, NULL,
                                    markdown_file_saved_cb,
                                    g_steal_pointer (&task));
}

gboolean
spiel_buffer_save_finish (SpielBuffer   *self,
                          GAsyncResult  *result,
                          GError       **error)
{
  g_return_val_if_fail (SPIEL_IS_BUFFER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_buffer_save, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
