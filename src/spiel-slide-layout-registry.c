/*
 * spiel-slide-layout-registry.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "Slide Layout Registry"

#include "spiel-ruleset-private.h"
#include "spiel-slide-feature-descriptor.h"
#include "spiel-slide-feature.h"
#include "spiel-slide-layout-registry-private.h"
#include "spiel-slide-layout-factory.h"
#include "spiel-slide-layout.h"

#include <libpeas.h>

struct _SpielSlideLayoutRegistry
{
  GObject parent_instance;

  PeasExtensionSet *layout_factories;

  GPtrArray *entries;
};

G_DEFINE_FINAL_TYPE (SpielSlideLayoutRegistry, spiel_slide_layout_registry, G_TYPE_OBJECT)

typedef struct
{
  GType type;
  SpielRuleset *ruleset;

  uint32_t score; /* Only used when matching */
} RegistryEntry;

static void
registry_entry_free (gpointer data)
{
  RegistryEntry *entry = (RegistryEntry *) data;

  if (!entry)
    return;

  g_clear_object (&entry->ruleset);
  g_free (entry);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (RegistryEntry, registry_entry_free);

/*
 * Callbacks
 */

static gpointer
copy_registry_entry_cb (gconstpointer src,
                        gpointer      data)
{
  g_autoptr (RegistryEntry) new_entry = NULL;
  const RegistryEntry *entry = src;

  new_entry = g_new0 (RegistryEntry, 1);
  new_entry->type = entry->type;
  new_entry->ruleset = g_object_ref (entry->ruleset);

  return g_steal_pointer (&new_entry);
}


/*
 * GObject overrides
 */

static void
spiel_slide_layout_registry_finalize (GObject *object)
{
  SpielSlideLayoutRegistry *self = (SpielSlideLayoutRegistry *)object;

  g_clear_pointer (&self->entries, g_ptr_array_unref);
  g_clear_object (&self->layout_factories);

  G_OBJECT_CLASS (spiel_slide_layout_registry_parent_class)->finalize (object);
}

static void
spiel_slide_layout_registry_class_init (SpielSlideLayoutRegistryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_slide_layout_registry_finalize;
}

static void
spiel_slide_layout_registry_init (SpielSlideLayoutRegistry *self)
{
  self->entries = g_ptr_array_new_with_free_func (registry_entry_free);
}

SpielSlideLayoutRegistry *
spiel_slide_layout_registry_new (void)
{
  return g_object_new (SPIEL_TYPE_SLIDE_LAYOUT_REGISTRY, NULL);
}

void
spiel_slide_layout_registry_load (SpielSlideLayoutRegistry *self)
{
  size_t n_factories;

  g_assert (SPIEL_IS_SLIDE_LAYOUT_REGISTRY (self));
  g_assert (self->layout_factories == NULL);

  self->layout_factories = peas_extension_set_new (peas_engine_get_default (),
                                                   SPIEL_TYPE_SLIDE_LAYOUT_FACTORY,
                                                   NULL);

  n_factories = g_list_model_get_n_items (G_LIST_MODEL (self->layout_factories));

  for (size_t i = 0; i < n_factories; i++)
    {
      g_autoptr (SpielSlideLayoutFactory) factory = NULL;

      factory = g_list_model_get_item (G_LIST_MODEL (self->layout_factories), i);
      spiel_slide_layout_factory_register_layouts (factory, self);
    }
}

GType
spiel_slide_layout_registry_match (SpielSlideLayoutRegistry *self,
                                   const SpielSlideFeature  *features[])
{
  g_autoptr (GPtrArray) entries = NULL;
  RegistryEntry *best_entry;
  size_t i = 0;

  entries = g_ptr_array_copy (self->entries, copy_registry_entry_cb, NULL);

  while (i < entries->len)
    {
      RegistryEntry *entry = g_ptr_array_index (entries, i);
      SpielRulesetMatchResult result;
      uint32_t score = 0;

      result = spiel_ruleset_calculate_score_for_features (entry->ruleset, features, &score);

      switch (result)
        {
        case SPIEL_RULESET_MATCH_RESULT_DENIED:
        case SPIEL_RULESET_MATCH_RESULT_UNMATCHED:
          g_ptr_array_remove_index_fast (entries, i);
          break;

        case SPIEL_RULESET_MATCH_RESULT_SCORED:
          entry->score = score;
          i++;
          break;

        default:
          g_assert_not_reached ();
        }
    }

  if (entries->len == 0)
    {
      g_warning ("No slide layout matches the current slide");
      return G_TYPE_NONE;
    }

  best_entry = NULL;
  for (i = 0; i < entries->len; i++)
    {
      RegistryEntry *entry = g_ptr_array_index (entries, i);

      if (!best_entry || best_entry->score < entry->score)
        best_entry = entry;
    }

  return best_entry->type;
}

/**
 * spiel_slide_layout_registry_register:
 * @self: a #SpielSlideLayoutRegistry
 * @ruleset: (transfer full): a ruleset for this slide type
 */
void
spiel_slide_layout_registry_register (SpielSlideLayoutRegistry  *self,
                                      GType                      slide_layout_type,
                                      SpielRuleset              *ruleset)
{
  g_autoptr (RegistryEntry) entry = NULL;

  g_return_if_fail (SPIEL_IS_SLIDE_LAYOUT_REGISTRY (self));
  g_return_if_fail (g_type_is_a (slide_layout_type, SPIEL_TYPE_SLIDE_LAYOUT));

  entry = g_new0 (RegistryEntry, 1);
  entry->type = slide_layout_type;
  entry->ruleset = ruleset;

  g_debug ("Adding %s to layout registry", g_type_name (slide_layout_type));

  g_ptr_array_add (self->entries, g_steal_pointer (&entry));
}
