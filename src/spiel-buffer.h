/*
 * spiel-buffer.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtksourceview/gtksource.h>

#include "spiel-types.h"

G_BEGIN_DECLS

#define SPIEL_TYPE_BUFFER (spiel_buffer_get_type())
G_DECLARE_FINAL_TYPE (SpielBuffer, spiel_buffer, SPIEL, BUFFER, GtkSourceBuffer)

GtkTextBuffer * spiel_buffer_new (SpielProject *project);

GtkTextBuffer * spiel_buffer_new_empty (void);

void     spiel_buffer_load        (SpielBuffer          *self,
                                   GCancellable         *cancellable,
                                   GAsyncReadyCallback   callback,
                                   gpointer              user_data);

gboolean spiel_buffer_load_finish (SpielBuffer          *self,
                                   GAsyncResult         *result,
                                   GError              **error);

void     spiel_buffer_save        (SpielBuffer         *self,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data);

gboolean spiel_buffer_save_finish (SpielBuffer   *self,
                                   GAsyncResult  *result,
                                   GError       **error);

G_END_DECLS
