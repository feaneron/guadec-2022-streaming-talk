/*
 * spiel-theme-selector.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-theme-selector.h"

#include "spiel-application.h"

struct _SpielThemeSelector
{
  AdwBin parent;

  GtkCheckButton *dark;
  GtkCheckButton *follow;
  GtkCheckButton *light;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (SpielThemeSelector, spiel_theme_selector, ADW_TYPE_BIN)


/*
 * Auxiliary methods
 */

static void
update_selected_check_button (SpielThemeSelector *self)
{
  AdwStyleManager *style_manager = adw_style_manager_get_default ();

  switch (adw_style_manager_get_color_scheme (style_manager))
    {
    case ADW_COLOR_SCHEME_DEFAULT:
      gtk_check_button_set_active (self->follow, TRUE);
      break;

    case ADW_COLOR_SCHEME_PREFER_DARK:
    case ADW_COLOR_SCHEME_FORCE_DARK:
      gtk_check_button_set_active (self->dark, TRUE);
      break;

    case ADW_COLOR_SCHEME_PREFER_LIGHT:
    case ADW_COLOR_SCHEME_FORCE_LIGHT:
      gtk_check_button_set_active (self->light, TRUE);
      break;

    default:
      g_assert_not_reached ();
    }
}


/*
 * Callbacks
 */

static void
on_style_manager_color_scheme_changed_cb (AdwStyleManager    *style_manager,
                                          GParamSpec         *pspec,
                                          SpielThemeSelector *self)
{
  update_selected_check_button (self);
}

static void
on_check_button_active_changed_cb (GtkCheckButton     *check_button,
                                   GParamSpec         *pspec,
                                   SpielThemeSelector *self)
{
  const char *variant;

  if (!gtk_check_button_get_active (check_button))
    return;

  if (check_button == self->dark)
    variant = "dark";
  else if (check_button == self->follow)
    variant = "default";
  else if (check_button == self->light)
    variant = "light";
  else
    g_assert_not_reached ();

  g_settings_set_string (self->settings, "style-variant", variant);
}


/*
 * GObject overrides
 */

static void
spiel_theme_selector_finalize (GObject *object)
{
  SpielThemeSelector *self = (SpielThemeSelector *)object;

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (spiel_theme_selector_parent_class)->finalize (object);
}

static void
spiel_theme_selector_class_init (SpielThemeSelectorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = spiel_theme_selector_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-theme-selector.ui");

  gtk_widget_class_bind_template_child (widget_class, SpielThemeSelector, dark);
  gtk_widget_class_bind_template_child (widget_class, SpielThemeSelector, follow);
  gtk_widget_class_bind_template_child (widget_class, SpielThemeSelector, light);

  gtk_widget_class_set_css_name (widget_class, "themeselector");
}

static void
spiel_theme_selector_init (SpielThemeSelector *self)
{
  SpielApplication *application;
  AdwStyleManager *style_manager;

  application = SPIEL_APPLICATION (g_application_get_default ());
  self->settings = spiel_application_get_settings (application);

	gtk_widget_init_template (GTK_WIDGET (self));

  update_selected_check_button (self);

  g_signal_connect (self->dark, "notify::active", G_CALLBACK (on_check_button_active_changed_cb), self);
  g_signal_connect (self->follow, "notify::active", G_CALLBACK (on_check_button_active_changed_cb), self);
  g_signal_connect (self->light, "notify::active", G_CALLBACK (on_check_button_active_changed_cb), self);

  style_manager = adw_style_manager_get_default ();
  g_signal_connect (style_manager, "notify::color-scheme", G_CALLBACK (on_style_manager_color_scheme_changed_cb), self);
}
