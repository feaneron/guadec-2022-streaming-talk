/* spiel-application.h
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "spiel-types.h"

G_BEGIN_DECLS

#define SPIEL_TYPE_APPLICATION (spiel_application_get_type())
#define SPIEL_APPLICATION_DEFAULT (SPIEL_APPLICATION (g_application_get_default()))

G_DECLARE_FINAL_TYPE (SpielApplication, spiel_application, SPIEL, APPLICATION, AdwApplication)

SpielApplication *spiel_application_new (const char        *application_id,
                                         GApplicationFlags  flags);

GSettings * spiel_application_get_settings (SpielApplication *self);

SpielProjectList * spiel_application_get_project_list (SpielApplication *self);

GtkWindow * spiel_application_find_project_window (SpielApplication *self,
                                                   SpielProject     *project);

SpielSlideLayoutRegistry * spiel_application_get_layout_registry (SpielApplication *self);

G_END_DECLS
