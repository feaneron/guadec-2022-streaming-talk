/*
 * spiel-slide-extractor.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "spiel-types.h"

G_BEGIN_DECLS

#define SPIEL_TYPE_SLIDE_EXTRACTOR (spiel_slide_extractor_get_type())
G_DECLARE_FINAL_TYPE (SpielSlideExtractor, spiel_slide_extractor, SPIEL, SLIDE_EXTRACTOR, GObject)

SpielSlideExtractor * spiel_slide_extractor_new (SpielBuffer *buffer);

GListModel * spiel_slide_extractor_get_slides (SpielSlideExtractor *self);

G_END_DECLS
