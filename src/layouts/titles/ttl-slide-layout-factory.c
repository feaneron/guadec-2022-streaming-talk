/*
 * ttl-slide-layout-factory.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-layout-factory.h"
#include "spiel-slide-layout-registry.h"
#include "spiel-ruleset.h"
#include "ttl-slide-layout-factory.h"
#include "ttl-title-layout.h"


struct _TtlSlideLayoutFactory
{
  PeasExtensionBase parent_instance;
};

static void spiel_slide_layout_factory_interface_init (SpielSlideLayoutFactoryInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (TtlSlideLayoutFactory, ttl_slide_layout_factory, PEAS_TYPE_EXTENSION_BASE,
                               G_IMPLEMENT_INTERFACE (SPIEL_TYPE_SLIDE_LAYOUT_FACTORY,
                                                      spiel_slide_layout_factory_interface_init))

static void
ttl_slide_layout_factory_register_layouts (SpielSlideLayoutFactory  *factory,
                                           SpielSlideLayoutRegistry *registry)
{
  spiel_slide_layout_registry_register (registry,
                                        TTL_TYPE_TITLE_LAYOUT,
                                        spiel_ruleset_new ((const char * const []) {
                                                             "title(>=1)",
                                                             NULL,
                                                           },
                                                           NULL,
                                                           NULL,
                                                           NULL));
}

static void
spiel_slide_layout_factory_interface_init (SpielSlideLayoutFactoryInterface *iface)
{
  iface->register_layouts = ttl_slide_layout_factory_register_layouts;
}

static void
ttl_slide_layout_factory_class_init (TtlSlideLayoutFactoryClass *klass)
{
}

static void
ttl_slide_layout_factory_init (TtlSlideLayoutFactory *self)
{
}
