/*
 * spiel-slide-feature-descriptor.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

#include "spiel-types.h"

G_BEGIN_DECLS

typedef enum
{
  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_UNKNOWN_FEATURE,
} SpielSlideFeatureDescriptorError;

#define SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR (spiel_slide_feature_descriptor_error_quark ())

#define SPIEL_TYPE_SLIDE_FEATURE_DESCRIPTOR (spiel_slide_feature_descriptor_get_type())
G_DECLARE_FINAL_TYPE (SpielSlideFeatureDescriptor, spiel_slide_feature_descriptor, SPIEL, SLIDE_FEATURE_DESCRIPTOR, GObject)

GQuark spiel_slide_feature_descriptor_error_quark (void);

SpielSlideFeatureDescriptor *
spiel_slide_feature_descriptor_from_string (const char  *descriptor,
                                            GError     **error);

gboolean
spiel_slide_feature_descriptor_match (SpielSlideFeatureDescriptor *self,
                                      const SpielSlideFeature     *feature);

gboolean
spiel_slide_feature_descriptor_equal (gconstpointer a,
                                      gconstpointer b);

G_END_DECLS
