/* spiel-window.h
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>

#include "spiel-types.h"

G_BEGIN_DECLS

#define SPIEL_TYPE_WINDOW (spiel_window_get_type())
G_DECLARE_FINAL_TYPE (SpielWindow, spiel_window, SPIEL, WINDOW, AdwApplicationWindow)

GtkWindow * spiel_window_new (SpielApplication *application,
                              SpielProject     *project);

SpielProject * spiel_window_get_project (SpielWindow *self);

G_END_DECLS
