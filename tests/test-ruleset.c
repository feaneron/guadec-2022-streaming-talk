/*
 * test-ruleset.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-ruleset-private.h"

/******************************************************************************/

static void
ruleset_construct (void)
{
  g_autoptr (SpielRuleset) ruleset = NULL;
  g_autoptr (GError) error = NULL;

  ruleset = spiel_ruleset_new ((const char *const []) { "title(>=1)", NULL },
                               NULL,
                               NULL,
                               &error);
  g_assert_no_error (error);
  g_assert_nonnull (ruleset);
}

/******************************************************************************/

static void
ruleset_empty (void)
{
  g_autoptr (SpielRuleset) ruleset = NULL;
  g_autoptr (GError) error = NULL;

  ruleset = spiel_ruleset_new (NULL, NULL, NULL, &error);
  g_assert_error (error, SPIEL_RULESET_ERROR, SPIEL_RULESET_ERROR_NO_RULES);
  g_assert_null (ruleset);
}

/******************************************************************************/

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, G_TEST_OPTION_ISOLATE_DIRS, NULL);

  g_test_add_func ("/ruleset/construct", ruleset_construct);
  g_test_add_func ("/ruleset/empty", ruleset_empty);

  return g_test_run ();
}
