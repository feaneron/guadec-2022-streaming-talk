/*
 * test-slide-features.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-feature-private.h"

static const struct {
  SpielFeatureType type;

  gboolean shallow_compare;
  gboolean deep_compare;

  union {

    struct {
      const char *text;
      uint32_t level;
    } title;

    struct {
      const char *text;
    } note;

  } a, b;

} features[] = {

  /* Title */
  {
    .type = SPIEL_FEATURE_TITLE,
    .shallow_compare = TRUE,
    .deep_compare = TRUE,
    .a = { .title.text = "Foo", .title.level = 1 },
    .b = { .title.text = "Foo", .title.level = 1 },
  },
  {
    .type = SPIEL_FEATURE_TITLE,
    .shallow_compare = TRUE,
    .deep_compare = FALSE,
    .a = { .title.text = "Foo", .title.level = 1 },
    .b = { .title.text = "Bar", .title.level = 1 },
  },
  {
    .type = SPIEL_FEATURE_TITLE,
    .shallow_compare = FALSE,
    .deep_compare = FALSE,
    .a = { .title.text = "Foo", .title.level = 1 },
    .b = { .title.text = "Foo", .title.level = 4 },
  },
  {
    .type = SPIEL_FEATURE_TITLE,
    .shallow_compare = FALSE,
    .deep_compare = FALSE,
    .a = { .title.text = "Foo", .title.level = 1 },
    .b = { .title.text = "Bar", .title.level = 4 },
  },

  /* Note */
  {
    .type = SPIEL_FEATURE_NOTE,
    .shallow_compare = TRUE,
    .deep_compare = TRUE,
    .a = { .note.text = "Foo" },
    .b = { .note.text = "Foo" },
  },
  {
    .type = SPIEL_FEATURE_NOTE,
    .shallow_compare = TRUE,
    .deep_compare = FALSE,
    .a = { .note.text = "Foo" },
    .b = { .note.text = "Bar" },
  },

};

/******************************************************************************/

static void
slide_features_compare_shallow (void)
{
  for (size_t i = 0; i < G_N_ELEMENTS (features); i++)
    {
      g_autoptr (SpielSlideFeature) feature_a = NULL;
      g_autoptr (SpielSlideFeature) feature_b = NULL;

      switch (features[i].type)
        {
        case SPIEL_FEATURE_TITLE:
          feature_a = spiel_slide_feature_new_title (g_strdup (features[i].a.title.text),
                                                     features[i].a.title.level);
          feature_b = spiel_slide_feature_new_title (g_strdup (features[i].b.title.text),
                                                     features[i].b.title.level);
          break;

        case SPIEL_FEATURE_NOTE:
          feature_a = spiel_slide_feature_new_note (g_strdup (features[i].a.note.text));
          feature_b = spiel_slide_feature_new_note (g_strdup (features[i].b.note.text));
          break;

        case SPIEL_FEATURE_MEDIA:
        default:
          g_assert_not_reached ();
        }

      g_assert_nonnull (feature_a);
      g_assert_cmpint (spiel_slide_feature_get_feature_type (feature_a), ==, features[i].type);

      g_assert_nonnull (feature_b);
      g_assert_cmpint (spiel_slide_feature_get_feature_type (feature_b), ==, features[i].type);

      if (features[i].shallow_compare)
        g_assert_true (spiel_slide_feature_equal_shallow (feature_a, feature_b));
      else
        g_assert_false (spiel_slide_feature_equal_shallow (feature_a, feature_b));
    }
}

/******************************************************************************/

static void
slide_features_compare_deep (void)
{

  for (size_t i = 0; i < G_N_ELEMENTS (features); i++)
    {
      g_autoptr (SpielSlideFeature) feature_a = NULL;
      g_autoptr (SpielSlideFeature) feature_b = NULL;

      switch (features[i].type)
        {
        case SPIEL_FEATURE_TITLE:
          feature_a = spiel_slide_feature_new_title (g_strdup (features[i].a.title.text),
                                                     features[i].a.title.level);
          feature_b = spiel_slide_feature_new_title (g_strdup (features[i].b.title.text),
                                                     features[i].b.title.level);
          break;

        case SPIEL_FEATURE_NOTE:
          feature_a = spiel_slide_feature_new_note (g_strdup (features[i].a.note.text));
          feature_b = spiel_slide_feature_new_note (g_strdup (features[i].b.note.text));
          break;

        case SPIEL_FEATURE_MEDIA:
        default:
          g_assert_not_reached ();
        }

      g_assert_nonnull (feature_a);
      g_assert_cmpint (spiel_slide_feature_get_feature_type (feature_a), ==, features[i].type);

      g_assert_nonnull (feature_b);
      g_assert_cmpint (spiel_slide_feature_get_feature_type (feature_b), ==, features[i].type);

      if (features[i].deep_compare)
        g_assert_true (spiel_slide_feature_equal_deep (feature_a, feature_b));
      else
        g_assert_false (spiel_slide_feature_equal_deep (feature_a, feature_b));
    }
}

/******************************************************************************/

static void
slide_features_copy (void)
{

  for (size_t i = 0; i < G_N_ELEMENTS (features); i++)
    {
      g_autoptr (SpielSlideFeature) feature = NULL;
      g_autoptr (SpielSlideFeature) copy = NULL;

      switch (features[i].type)
        {
        case SPIEL_FEATURE_TITLE:
          feature = spiel_slide_feature_new_title (g_strdup (features[i].a.title.text),
                                                   features[i].a.title.level);
          break;

        case SPIEL_FEATURE_NOTE:
          feature = spiel_slide_feature_new_note (g_strdup (features[i].a.note.text));
          break;

        case SPIEL_FEATURE_MEDIA:
        default:
          g_assert_not_reached ();
        }

      g_assert_nonnull (feature);
      g_assert_cmpint (spiel_slide_feature_get_feature_type (feature), ==, features[i].type);

      copy = spiel_slide_feature_copy (feature);

      g_assert_nonnull (copy);
      g_assert_cmpint (spiel_slide_feature_get_feature_type (feature), ==, features[i].type);
      g_assert_true (spiel_slide_feature_equal_deep (feature, copy));
    }
}

/******************************************************************************/

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, G_TEST_OPTION_ISOLATE_DIRS, NULL);

  g_test_add_func ("/slide/features/compare-shallow", slide_features_compare_shallow);
  g_test_add_func ("/slide/features/compare-deep", slide_features_compare_deep);
  g_test_add_func ("/slide/features/copy", slide_features_copy);

  return g_test_run ();
}
